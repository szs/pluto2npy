pluto2npy
=========

Convert PLUTO (http://plutocode.ph.unito.it/) output files to numpy npy files.

Usage
-----

.. code-block:: shell

                p2n data.0001.dbl
